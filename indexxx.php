<?php
  error_reporting (E_ALL & ~ E_NOTICE & ~ E_DEPRECATED); //IGNORE ESTA LINHA

  session_name('sistema');
  session_cache_limiter('private, must-revalidate');
  session_start();

  include('conecta.php');

  // Faz a decodificacao da acentuacao mo Banco de Dados
  header('Content-Type: text/html; charset=utf-8');
  mysql_query("SET NAMES 'utf8'");
  mysql_query('SET character_set_connection=utf8');
  mysql_query('SET character_set_client=utf8');
  mysql_query('SET character_set_results=utf8');

  $aliasBanco= "";

    //echo "PASSOU: " . $idTpProduto . "<br>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>MutCOM - 8º Distrito Escoteiro</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1, minimum-scale=1">
<link rel="stylesheet" type="text/css" href="css/default.css" media="all">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans">
<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="js/jquery.flexslider.js" type="text/javascript"></script>
<script src="js/default.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" language="javascript">

  function SomenteNumero(e){
      var tecla=(window.event)?event.keyCode:e.which;

      if((tecla>47 && tecla<58)) return true;
      else{
        if (tecla==8 || tecla==0) return true;
        else  return false;
      }
  }

  function validarForm(){

    var vtnome      = document.CadastrarDados.vt_nome.value;
    var vtmatricula = document.CadastrarDados.vt_matricula.value;
    var vtgrupo     = document.CadastrarDados.vt_grupo.value;
    var vtbase      = document.CadastrarDados.vt_base.value;
    var vtramo      = document.CadastrarDados.vt_ramo.value;

    if(document.CadastrarDados.vt_nome.value==""){
      alert( "O campo 'Nome Completo' deve ser preenchido!" );
      return false;
      document.CadastrarDados.vt_nome.focus();
    }

    if(document.CadastrarDados.vt_matricula.value==""){
      alert( "O campo 'Nome Completo' deve ser preenchido!" );
      return false;
      document.CadastrarDados.vt_matricula.focus();
    }

    if ((vtgrupo == null) || (vtgrupo == "")){
      alert("O campo 'Grupo Escoteiro' deve ser preenchido!");
      return false;
    }

    if ((vtbase == null) || (vtbase == "")){
      alert("O campo 'Base de Preferência' deve ser preenchido!");
      return false;
    }

    if ((vtramo == null) || (vtramo == "")){
      alert("O campo 'Ramo' deve ser preenchido!");
      return false;
    }

    document.CadastrarDados.vt_passou.value = "yes";
    document.CadastrarDados.submit();

  }

</script>

</head>
<body>
<div id="pagewidth">
  <header id="header">
      <div class="center">
        <img src="img/logotipo.fw.png" width="20%">
      </div>
  </header>
  <div id="content">
    <section class="row">
      <div class="center">
        <h1>18º MutCom - 8º Distrito Escoteiro</h1>
        <strong class="subHeading">Formulário de Seleção de Bases</strong>
        <hr>

        <?php
          if ( $_POST['vt_passou']=="yes" ) {

            // VERIFICA SE O USUÁRIO JÁ ESTA CADASTRADO NO SISTEMA
            $SqlConfirma= "select numeroUEB from tbcadastro where numeroUEB = '" . $_POST['vt_matricula'] . "' ";
            $ResultConfirma= mysql_query($SqlConfirma,$conexao);
            if (mysql_num_rows($ResultConfirma) > 0){
              echo "
                <script type=\"text/javascript\">
                  alert('Já existe um cadastro para o Número UEB (Sigue): " . $_POST['vt_matricula'] . ". Não é possível efetuar mais de um cadastro.');
                </script>";
            }else {

              // CADASTRA O USUÁRIO
              $SqlCadEscot="insert into tbcadastro (nome, numeroUEB, grupoEscoteiro, basePreferencia, ramo, status, dataInclusao) values ('" . $_POST['vt_nome'] . "'," . $_POST['vt_matricula'] . ",'" . $_POST['vt_grupo'] . "','" . $_POST['vt_base'] . "','" . $_POST['vt_ramo'] . "',0, now()) ";
              //echo $SqlCadEscot;
              $ResultCadEscot= mysql_query($SqlCadEscot,$conexao);

              // CONFIRMA SE A MATRICULA FOI INCLUIDA CORRETAMENTE NO BANCO
              $SqlReConfirma= "select numeroUEB from tbcadastro where numeroUEB = '" . $_POST['vt_matricula'] . "' ";
              $ResultReConfirma= mysql_query($SqlReConfirma,$conexao);
              if (mysql_num_rows($ResultReConfirma) > 0){
                echo "<div id=cadOk>Obrigado! Seu cadastro foi incluído com sucesso. SAPS.</div>";
              } else{
                echo "<div id=cadOk>OPS! Ocorreu um erro durante a inclusão de seu cadastro. Favor tentar novamente!</div>";
              }
            }

          }
        ?>

        <form action="index.php" method="POST" name="CadastrarDados">
          <input type="hidden" name="vt_passou" value="">
          <table>
            <tr>
              <td>Nome Completo</td>
              <td>Número UEB (Sigue)</td>
              <td>Ramo</td>
              <td>Grupo Escoteiro</td>
              <td>Base de Preferência</td>
            </tr>
            <tr>
              <td><input type="text" name="vt_nome" size="20"></td>
              <td><input type="text" name="vt_matricula" size="20" onkeypress='return SomenteNumero(event)' maxlength="8"></td>
              <td>
                <select name="vt_ramo">
                  <option value="">Selecionar ...</option>
                  <option value="Escoteiro">Escoteiro</option>
                  <option value="Sênior">Sênior</option>
                  <option value="Pioneiro">Pioneiro</option>
                </select>
              </td>
              <td>
                <select name="vt_grupo">
                  <option value="">Selecionar ...</option>
                  <option value="Charruas - 003 - RS">Charruas - 003 - RS</option>
                  <option value="Nimuendaju - 014 - RS">Nimuendaju - 014 - RS</option>
                  <option value="Bento Gonçalves - 015 - RS">Bento Gonçalves - 015 - RS</option>
                  <option value="Lidia Moschetti - 051 - RS">Lidia Moschetti - 051 - RS</option>
                  <option value="Marechal Osório - 056 - RS">Marechal Osório - 056 - RS</option>
                  <option value="Brownsea - 089 - RS">Brownsea - 089 - RS</option>
                  <option value="Do Mar Passo da Pátria - 090 - RS">Do Mar Passo da Pátria - 090 - RS</option>
                  <option value="Monsenhor André Pedro Frank - 099 - RS">Monsenhor André Pedro Frank - 099 - RS</option>
                  <option value="Urutu - 155 - RS">Urutu - 155 - RS</option>
                  <option value="Tupi-Guarani - 236 - RS">Tupi-Guarani - 236 - RS</option>
                  <option value="Lanceiros Negros - 333 - RS">Lanceiros Negros - 333 - RS</option>
                  <option value="Do Mar Seival - 365 - RS">Do Mar Seival - 365 - RS</option>
                </select>
              </td>
              <td>
                <select name="vt_base">
                  <option value="">Selecionar ...</option>
                  <option value="Escola Estadual Odilla Gay">(1) Escola Estadual Odilla Gay</option>
                  <!--<option value="Creche Nossa Senhora Aparecida ">(2) Creche Nossa Senhora Aparecida</option>-->
                  <option value="Escola Miguel Tostes">(3) Escola Miguel Tostes</option>
                  <!--<option value="Zaffari - Juca Batista">(4) Zaffari - Juca Batista</option>-->
                  <!--<option value="Orla do Guaíba">(5) Orla do Guaíba</option>-->
                </select>
              </td>
              <td cols="4"><button class="btnSmall btn submit right" onclick="return validarForm();"><span>Enviar</span></button></td>
            </tr>
          </table>
          <br><br><br><br>
        </form>
    </section>

    <section id="contactUs" class="row grey">
      <div class="center">
        <h2>Queremos ouvir você! <br><br>Sua opiniões, saber suas dúvidas, sugestões, críticas. SEMPRE ALERTA !!!</h2>
        <h3><a href="mailto:escoteiros8d.rs@gmail.com?Subject=18MutCOM">escoteiros8d.rs@gmail.com</a></h3>
      </div>
    </section>
  </div>
  <footer id="footer">
    <div class="center">&copy; Copyright 2016 - 8º Distrito Escoteiro.</div>
  </footer>
</div>
</body>
</html>

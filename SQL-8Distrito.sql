/*
SQLyog Community Edition- MySQL GUI v5.20
Host - 5.5.5-10.1.13-MariaDB : Database - mutcom
*********************************************************************
Server version : 5.5.5-10.1.13-MariaDB
*/


SET NAMES utf8;

SET SQL_MODE='';

create database if not exists `mutcom`;

USE `mutcom`;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `tbcadastro` */

DROP TABLE IF EXISTS `tbcadastro`;

CREATE TABLE `tbcadastro` (
  `objid` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `numeroUEB` int(8) NOT NULL,
  `grupoEscoteiro` varchar(50) DEFAULT NULL,
  `basePreferencia` varchar(50) DEFAULT NULL,
  `ramo` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `dataInclusao` datetime DEFAULT NULL,
  PRIMARY KEY (`objid`,`numeroUEB`),
  UNIQUE KEY `objid` (`objid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

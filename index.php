<!DOCTYPE html>
<html lang="en">
<head>
<title>MutCOM - 8º Distrito Escoteiro</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1, minimum-scale=1">
<link rel="stylesheet" type="text/css" href="css/default.css" media="all">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans">
<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="js/jquery.flexslider.js" type="text/javascript"></script>
<script src="js/default.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<div id="pagewidth">
  <header id="header">
      <div class="center">
        <img src="img/logotipo.fw.png" width="20%">
      </div>
  </header>
  <div id="content">
    <section class="row">
      <div class="center">
        <h1>18º MutCom - 8º Distrito Escoteiro</h1><br/><br />
        <h2 style="color: red; ">INSCRIÇÕES ENCERRADAS</h2><br />
        <h3>Obrigado a todos que se inscreveram. Nos vemos no MUTCOM!</h3>
</section>

    <section id="contactUs" class="row grey">
      <div class="center">
        <h2>Queremos ouvir você! <br><br>Sua opiniões, saber suas dúvidas, sugestões, críticas. SEMPRE ALERTA !!!</h2>
        <h3><a href="mailto:escoteiros8d.rs@gmail.com?Subject=18MutCOM">escoteiros8d.rs@gmail.com</a></h3>
      </div>
    </section>
  </div>
  <footer id="footer">
    <div class="center">&copy; Copyright 2016 - 8º Distrito Escoteiro.</div>
  </footer>
</div>
</body>
</html>
